﻿using System;
using System.Collections;
using System.Collections.Generic;
using Collections.Interfaces;

namespace Collections
{
	public class DynamicArray<T> : IDynamicArray<T>
	{
		private T[] array;
		private int Arraylength = 0; // Колличество элементов при значимом типе данных.

		public DynamicArray()
		{
			T defaultT = default;
			if(defaultT == null)
				array = new T[8];
			else
            {
				array = new T[8];
				Arraylength = 8;
			}
		}

		public DynamicArray(int capacity)
		{
			T defaultT = default;
			if (defaultT == null)
				array = new T[capacity];
			else
            {
				array = new T[capacity];
				Arraylength = capacity;
			}
		}

		public DynamicArray(IEnumerable<T> items)
		{
			T defaultT = default;
			if (defaultT == null)
            {
				array = collectionToMass(items);
			}
			else
            {
				Arraylength = collectionToMass(items).Length;
				array = collectionToMass(items);

			}
		}

		public IEnumerator<T> GetEnumerator()
		{
			foreach (T item in array)
			{
				yield return item;
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public T this[int index]
		{
			get 
			{
				if (index >  Length -1 || index < 0)
					throw new System.IndexOutOfRangeException("Индекс вышел за пределы массива");
				return array[index]; 
			}
			set
            {
				if (index > Capacity - 1 || index < 0)
					throw new System.IndexOutOfRangeException("Индекс вышел за пределы массива");
				array[index] = value;
			}
		}

		
		public int Length
		{
			get
			{
				T defaultT = default;
				if (defaultT == null) // Если работаем со ссылочным типом, то подсчет элементов ведем колличеством элементов в массве не равных null.
                {
					int result = 0;
					foreach (var i in collectionToMass(array))
					{
						if (i != null)
							result += 1;
					}
					return result;
				}


				return Arraylength;
			}

		}

		public int Capacity { get { return array.Length; } }


		public void Add(T item)
		{
			int reservIndexNullElem = IndexFirstNullElemInArray;
			if (Capacity == Length)
			{
				T[] savearray = array;

				if (array.Length == 0)
					array = new T[1];

				else
				{
					array = new T[(array.Length) * 2];
				}
				savearray.CopyTo(array, 0);
				array[reservIndexNullElem] = item;
				return;
			}
			array[reservIndexNullElem] = item;
		}


		public void AddRange(IEnumerable<T> items)
		{
			if (collectionToMass(items).Length == 0)
            {
				return;
            }

			T defaultT = default;
			T[] reservArray = array;
			int reservArrayLength = array.Length;

			if (defaultT == null) // Работаем со ссылочными типами.
            {
				int kolNotNullElemInItems = 0;
				foreach (var item in items)
				{
					if (item != null)
					{
						kolNotNullElemInItems += 1; // Получае количество проинициализированных элементов в массиве.
					}
				}
				if (Capacity < Length + kolNotNullElemInItems) // Если array не имеет достаочное количество непроинициализированных элементов
				{											   // То увеличиваем размер массива в 2 раза, пока места не хватит.
					int newArrayLength = reservArrayLength * 2;
					while (true)
					{
						if (newArrayLength > Length + collectionToMass(items).Length - 1)
						{
							array = new T[newArrayLength];
							reservArray.CopyTo(array, 0);

							int index = IndexFirstNullElemInArray;

							foreach (var item in items)
							{
								if (item != null) // После того, как мы получили массив нужной длины, и добавляем в него все старые элементы из array
								{				  // Далее начиная с первого пустого элемента копируем коллекцию.
									array[index] = item;
									index += 1;
								}
							}
							break;
						}
						newArrayLength *= 2;
					}
					return;
				}
				array = new T[reservArrayLength]; // В случае если в массиве достаточное количество непроинициализированных переменных
				array.CopyTo(reservArray, 0);	  // Копируем коллекцию начиная с первого непроинициализированного элемента.
				int indexToFinalArray = IndexFirstNullElemInArray;
				foreach (var item in items)
				{
					if (item != null)
					{
						array[indexToFinalArray] = item;
						indexToFinalArray += 1;
					}
				}
			}

            else // Работаем со значимыми типами.
            {
				int newArrayLength = reservArrayLength * 2; // Здесь всегда все переменные проициализированны, поэтму сразу умножаем количество элементов в 2 раза.
				while (true)
                {
					if(newArrayLength > reservArrayLength + collectionToMass(items).Length - 1) //Если количество элементов позволяет копировать коллекцию, то копируем
                    {
						Arraylength = reservArrayLength + collectionToMass(items).Length;
						array = new T[newArrayLength];
						reservArray.CopyTo(array, 0);
						collectionToMass(items).CopyTo(array, reservArrayLength);
						break;
                    }
					newArrayLength *= 2; // Умножаем еще в два раза пока количество элементов не станет достаточным для копирования коллекции.
				}
				return;

            }
		}

		public void Insert(T item, int index)
		{
			if (index > Capacity - 1 || index < 0)
				throw new System.IndexOutOfRangeException("Индекс вышел за пределы массива.");
			array[index] = item;
		}

		public bool Remove(T item)
		{
			if (array.Length == 0)
				return false;
			if (item == null)
				return false;

			bool itemRemoved = false; // Используем данную переменную, чтобы понимать произошло удаление или нет.
			T[] newMass = array; // Резервируем массив для работы с ним.
			int index = 0;
			T defaultItem = default;
			if (defaultItem == null)
			{
				for (int ind = 0; ind < Length; ind++)
				{
					if (array[ind].Equals(item))
					{
						newMass[ind] = default;
						itemRemoved = true;
						Arraylength -= 1;
						break;
					}
				}
			}
			else
			{
				foreach (var elem in array)
				{
					if (elem.Equals(item))
					{
						newMass[index] = default;
						itemRemoved = true;
						break;
					}
					index++;
				}
			}

			if (itemRemoved == false)
				return false;

			T defaultT = default;
			if (defaultT == null) // Данное условие пришлось добавить из за ошибки в юнит тесте (при прохождении через элементы с помощью
			{                     // var afterRemoveCount = dynamicArray.Count(item => item.Equals(itemToRemove));
				int kolNotNullElem = 0;    // item Здесь задается поиск по элементам массива, после удаления из него элемента,
										   // item соответственно принимает значения всех элементов массива по очереди и сравнивает их с itemToRemove,
				foreach (var elem in newMass)    // но в dynamicArray исходя из условия могут быть элементы содержащие null и когда item принимает их 
				{                                // значение, тест уходит в ексепшн. Соответственно итоговый массив должен быть пересоздан без null значений
					if (elem != null)            // ( но в условии указано так: "Например, при удалении из массива {1, 2, 3, 4, 5} элемента 3 новый массив
					{                            //  должен иметь вид {1, 2, 4, 5, <пусто>}, а не {1, 2, <пусто>, 4, 5}").
						kolNotNullElem += 1;
                    }
                }


                T[] resultRef = new T[kolNotNullElem];
				int indexResultRef = 0;
				foreach (var elem in newMass) // Добавляем элементы из полученного массива в конечный массив.
				{
					if (elem != null) // Делаем это только тогда, когда элемент не равен null, чтобы все непроинициализированные элементы остались в конце.
					{
						resultRef[indexResultRef] = elem;
						indexResultRef++;
					}
				}
				array = resultRef;
				return true;
			}

			if (itemRemoved == true)
			{
				Arraylength -= 1;
				return true;
			}
			return false;
		}

		private int IndexFirstNullElemInArray
		{
			get
			{
				T defaultT = default;

				if (defaultT == null)
				{
					int index = -1;
					foreach (var i in array)
					{
						if (i != null)
						{
							if (index == -1)
								index += 1;
							index += 1;
						}
					}
					if (index == -1)
						return 0;
					return index;
				}
				return array.Length;

			}
		}

		private string[] arrayToStringArray(T[] arr)
		{
			string[] result = new string[arr.Length];
			for (int i = 0; i < arr.Length; i++)
			{
				if (arr[i] != null)
				{
					result[i] = arr[i].ToString();
				}
			}
			return result;
		}

		private T[] collectionToMass(IEnumerable<T> items)
		{
			int kolElem = 0;
			foreach (var i in items)
			{
				kolElem += 1;
			}
			T[] arr = new T[kolElem];
			int index = 0;
			foreach (var i in items)
			{
				arr[index] = i;
				index += 1;
			}
			T[] sortArray = new T[arr.Length];
			int indexSortArray = 0;
			foreach(var elem in arr)
            {
				if(elem!=null)
                {
					sortArray[indexSortArray] = elem;
                }
				indexSortArray += 1;
            }
			return sortArray;
		}
	}
}